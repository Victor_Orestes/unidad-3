import java.io.*;

import javax.swing.JOptionPane;

public class Textos {
	public void escribir(String nomArchivo)
	{
		File f; 
		f = new File(nomArchivo);
		try
		{
			FileWriter w = new FileWriter(f);
			BufferedWriter bw = new BufferedWriter (w);
			PrintWriter wr = new PrintWriter(bw);
			String captura = JOptionPane.showInputDialog("Introduce una cadena de texto.");
			wr.write(captura);
			//wr.append(" - esto una concatenación al texto");
			wr.close();
			bw.close();
		}
		catch(Exception ex)
		{
			ex.getMessage();
		}
	}
	public void leer(String archivo)
	{
		try 
		{
			FileReader r = new FileReader(archivo);
			BufferedReader buffer = new BufferedReader(r);
			String temp="";
			while(temp!=null)
			{
				temp = buffer.readLine();
				if(temp==null)
					break;	
				System.out.println(temp);
			}
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
	}
}
