package paquete;
import javax.swing.*;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.event.*;
import java.awt.*;
public class Form  extends JFrame implements ActionListener	{
    private JMenuBar mb;
    private JMenu menu1;
    private JMenuItem mi1,mi2;
    private JTextField tf1,tf2;
    
    private JButton varon, mujer;
    
    private JLabel et1, et2, et3;
    
    private JTextField user,pass;
    private JLabel userEt,passEt;
    private JButton aceptarUser;
    
    private JTextArea area1,area2;
    private JButton verificar;
    
    private JLabel comboEtUser,comboEtPais;
    private JTextField userCombo;
    private JComboBox pais;
    private JButton comboBtn;
    
    private JCheckBox check1,check2,check3;
    private JButton checkBtn;
    
	public Form()
	{
        //Men� general para problemas propuestos
		setLayout(null);
        mb=new JMenuBar();
        setJMenuBar(mb);
        menu1=new JMenu("Opciones");
        mb.add(menu1);
        mi1=new JMenuItem("Redimensionar ventana");
        menu1.add(mi1);
        mi1.addActionListener(this);
        mi2=new JMenuItem("Salir");
        menu1.add(mi2);
        mi2.addActionListener(this);
        tf1=new JTextField();
        tf1.setBounds(600,10,100,30);
        add(tf1);
        tf2=new JTextField();
        tf2.setBounds(600,50,100,30);
        add(tf2);

        //--------------------
        
        //Crear tres objetos de la clase JLabel, ubicarlos uno debajo de otro y mostrar nombres de colores.
        et1=new JLabel("Azul");
        et2=new JLabel("Verde");
        et3=new JLabel("Rojo");
        et1.setBounds(10,10,100,20);
        et2.setBounds(10,40,100,20);
        et3.setBounds(10,70,100,20);
        add(et1);
        add(et2);
        add(et3);
        
        //Disponer dos objetos de la clase JButton con las etiquetas: "var�n" y "mujer", al presionarse 
        // mostrar en la barra de t�tulos del JFrame la etiqueta del bot�n presionado.
        varon = new JButton("Hombre");
        varon.setBounds(100, 10, 100, 30);
        mujer = new JButton("Mujer");
        mujer.setBounds(100, 50, 100, 30);
        add(varon);
        add(mujer);
        varon.addActionListener(this);
        mujer.addActionListener(this);
        
        //Ingresar el nombre de usuario y clave en controles de tipo JTextField. Si se ingresa las cadena 
        //(usuario: "juan", clave: "abc123") luego mostrar en el t�tulo del JFrame el mensaje "Correcto" en caso contrario mostrar el mensaje "Incorrecto".
        
        userEt = new JLabel("Usuario:");
        userEt.setBounds(10, 110, 100, 30);
        add(userEt);
        user = new JTextField();
        user.setBounds(110,110,100,30);
        add(user);
        passEt = new JLabel("Password:");
        passEt.setBounds(10,140,100,30);
        add(passEt);
        pass = new JTextField();
        pass.setBounds(110, 140, 100, 30);
        add(pass);
        aceptarUser = new JButton("Aceptar");
        aceptarUser.setBounds(50, 170, 100, 30);
        add(aceptarUser);
        aceptarUser.addActionListener(this);
        
        //Disponer dos controles de tipo JTextArea, luego al presionar un bot�n verificar si tienen exactamente el mismo contenido.
        area1 = new JTextArea();
        area1.setBounds(10,210,200,200);
        add(area1);
        area2 = new JTextArea();
        area2.setBounds(220,210,200,200);
        add(area2);
        verificar = new JButton("Verificar texto");
        verificar.setBounds(440,210,150,50);
        add(verificar);
        verificar.addActionListener(this);
        
        //Solicitar el ingreso del nombre de una persona y seleccionar de un control JComboBox un pa�s. Al presionar un bot�n 
        //mostrar en la barra del t�tulo del JFrame el nombre ingresado y el pa�s seleccionado.

        
        comboEtUser = new JLabel("Usuario");
        comboEtUser.setBounds(220,10,100,30);
        add(comboEtUser);
        comboEtPais = new JLabel("Pais");
        comboEtPais.setBounds(220,40,100,30);
        add(comboEtPais);
        userCombo = new JTextField();
        userCombo.setBounds(320,10,100,30);
        add(userCombo);
        pais = new JComboBox();
        pais.setBounds(320, 40, 100, 30);
        pais.addItem("M�xico");
        pais.addItem("Colombia");
        pais.addItem("Per�");
        pais.addItem("Argentina");
        pais.addItem("Chile");
        add(pais);
        comboBtn = new JButton("Confirmar");
        comboBtn.setBounds(430, 10, 100, 30);
        add(comboBtn);
        comboBtn.addActionListener(this);
        
        //Disponer tres objetos de la clase JCheckBox con nombres de navegadores web. Cuando se presione un bot�n mostrar en el t�tulo del JFrame los programas seleccionados.
        check1=new JCheckBox("Google Chrome");
        check1.setBounds(700,10,150,30);
        add(check1);
        check2=new JCheckBox("Mozilla FireFox");
        check2.setBounds(700,50,150,30);      
        add(check2);
        check3=new JCheckBox("Brave");
        check3.setBounds(700,90,150,30);        
        add(check3);               
        checkBtn=new JButton("Confirmar");
        checkBtn.setBounds(700,140,100,30);
        checkBtn.addActionListener(this);
        add(checkBtn);
        
	}
	public void actionPerformed(ActionEvent e)
	{
        if (e.getSource()==mi1) {
            String cad1=tf1.getText();
            String cad2=tf2.getText();
            try 
            {
            	int ancho=Integer.parseInt(cad1);
            	int alto=Integer.parseInt(cad2);
            	setSize(ancho,alto);
            }
            catch(Exception ex)
            {
            	setTitle("Datos incorrectos.");
            }
        }
        if (e.getSource()==mi2) {
            System.exit(0);
        }
        if (e.getSource()==varon)
        {
        	setTitle("Hombre");
        }
        if (e.getSource()==mujer)
        {
        	setTitle("Mujer");
        }
        if (e.getSource()==aceptarUser)
        {
        	String juan = "juan";
        	String abc = "abc123";
        	String usuario = user.getText();
        	String contrasena = pass.getText();
        	if(usuario.equals(juan))
        	{

        		if(contrasena.equals(abc))
        		{
        			setTitle("Correcto");
        		}
        		else
        		{
        			setTitle("Incorrecto");
        		}
        	}
    		else
    		{
    			setTitle("Incorrecto");
    		}
        }
        if (e.getSource()==verificar)
        {
        	String areaIzq = area1.getText();
        	String areaDer = area2.getText();
        	if(areaIzq.equals(areaDer))
        	{
        		JOptionPane.showMessageDialog(null, "Ambas �reas contienen el mismo texto");
        	}
        	else
        	{
        		JOptionPane.showMessageDialog(null, "El contenido de las �reas es distinto.");
        	}
        }
        if (e.getSource()==comboBtn)
        {
        	String name = userCombo.getText();
        	String country = (String)pais.getSelectedItem();
        	setTitle(name+" - "+country);
        }
        if (e.getSource()==checkBtn) 
        {
            String cad="";
            if (check1.isSelected()==true) {
                cad=cad+"Google Chrome-";
            }
            if (check2.isSelected()==true) {
                cad=cad+"Mozilla FireFox-";
            }
            if (check3.isSelected()==true) {
                cad=cad+"Brave-";
            }
            setTitle(cad);
        }
	}
	
}
