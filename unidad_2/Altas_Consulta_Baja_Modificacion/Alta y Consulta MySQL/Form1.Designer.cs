﻿namespace Alta_y_Consulta_MySQL
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.resultadoConsulta = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.descripciónTxt = new System.Windows.Forms.TextBox();
            this.precioTxt = new System.Windows.Forms.TextBox();
            this.consultaTxt = new System.Windows.Forms.TextBox();
            this.consultaBTN = new System.Windows.Forms.Button();
            this.altaBTN = new System.Windows.Forms.Button();
            this.formBTN = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(50, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(242, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Descripción del artículo:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(50, 138);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(79, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Precio:";
            // 
            // resultadoConsulta
            // 
            this.resultadoConsulta.AutoSize = true;
            this.resultadoConsulta.Location = new System.Drawing.Point(50, 429);
            this.resultadoConsulta.Name = "resultadoConsulta";
            this.resultadoConsulta.Size = new System.Drawing.Size(109, 25);
            this.resultadoConsulta.TabIndex = 2;
            this.resultadoConsulta.Text = "Resultado";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(50, 308);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(405, 25);
            this.label4.TabIndex = 3;
            this.label4.Text = "Ingrese el código del artículo a consultar:";
            // 
            // descripciónTxt
            // 
            this.descripciónTxt.Location = new System.Drawing.Point(307, 73);
            this.descripciónTxt.Name = "descripciónTxt";
            this.descripciónTxt.Size = new System.Drawing.Size(250, 31);
            this.descripciónTxt.TabIndex = 1;
            // 
            // precioTxt
            // 
            this.precioTxt.Location = new System.Drawing.Point(307, 135);
            this.precioTxt.Name = "precioTxt";
            this.precioTxt.Size = new System.Drawing.Size(150, 31);
            this.precioTxt.TabIndex = 2;
            // 
            // consultaTxt
            // 
            this.consultaTxt.Location = new System.Drawing.Point(466, 302);
            this.consultaTxt.Name = "consultaTxt";
            this.consultaTxt.Size = new System.Drawing.Size(200, 31);
            this.consultaTxt.TabIndex = 4;
            // 
            // consultaBTN
            // 
            this.consultaBTN.Location = new System.Drawing.Point(213, 353);
            this.consultaBTN.Name = "consultaBTN";
            this.consultaBTN.Size = new System.Drawing.Size(227, 50);
            this.consultaBTN.TabIndex = 5;
            this.consultaBTN.Text = "Consular por código";
            this.consultaBTN.UseVisualStyleBackColor = true;
            this.consultaBTN.Click += new System.EventHandler(this.ConsultaBTN_Click);
            // 
            // altaBTN
            // 
            this.altaBTN.Location = new System.Drawing.Point(141, 184);
            this.altaBTN.Name = "altaBTN";
            this.altaBTN.Size = new System.Drawing.Size(108, 50);
            this.altaBTN.TabIndex = 3;
            this.altaBTN.Text = "Alta";
            this.altaBTN.UseVisualStyleBackColor = true;
            this.altaBTN.Click += new System.EventHandler(this.AltaBTN_Click);
            // 
            // formBTN
            // 
            this.formBTN.BackColor = System.Drawing.Color.Blue;
            this.formBTN.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.formBTN.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.125F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.formBTN.ForeColor = System.Drawing.Color.White;
            this.formBTN.Location = new System.Drawing.Point(628, 3);
            this.formBTN.Name = "formBTN";
            this.formBTN.Size = new System.Drawing.Size(76, 49);
            this.formBTN.TabIndex = 6;
            this.formBTN.Text = "2";
            this.formBTN.UseVisualStyleBackColor = false;
            this.formBTN.Click += new System.EventHandler(this.FormBTN_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(706, 487);
            this.Controls.Add(this.formBTN);
            this.Controls.Add(this.altaBTN);
            this.Controls.Add(this.consultaBTN);
            this.Controls.Add(this.consultaTxt);
            this.Controls.Add(this.precioTxt);
            this.Controls.Add(this.descripciónTxt);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.resultadoConsulta);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Alta y Consulta de MySQL";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label resultadoConsulta;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox descripciónTxt;
        private System.Windows.Forms.TextBox precioTxt;
        private System.Windows.Forms.TextBox consultaTxt;
        private System.Windows.Forms.Button consultaBTN;
        private System.Windows.Forms.Button altaBTN;
        private System.Windows.Forms.Button formBTN;
    }
}

