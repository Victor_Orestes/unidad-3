﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_Strategy
{
    class EstrategiasDelBorrachoContexto
    {
        private IBorracho oBorracho;

        public EstrategiasDelBorrachoContexto()
        {
            this.oBorracho = new EstrategiaOjitos();
            
        }
        public void EstablecerConquistaOjitos()
        {
            this.oBorracho = new EstrategiaOjitos();
        }
        public void EstablecerConquistaInvitarCerveza()
        {
            this.oBorracho = new EstrategiaInvitarCerveza();
        }
        public void Conquistar()
        {
            this.oBorracho.Conquistar();
        }
    }
}
