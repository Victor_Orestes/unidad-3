﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_Strategy
{
    class Program
    {
        static void Main(string[] args)
        {
            EstrategiasDelBorrachoContexto oBorracho = new EstrategiasDelBorrachoContexto();
            oBorracho.Conquistar();
            oBorracho.EstablecerConquistaInvitarCerveza();
            oBorracho.Conquistar();
        }
    }
}
