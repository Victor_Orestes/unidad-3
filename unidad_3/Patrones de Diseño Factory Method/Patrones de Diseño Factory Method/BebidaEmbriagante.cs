﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_Factory_Method
{
    public abstract class BebidaEmbriagante
    {
        public abstract int CuentoMeEmbriagaPorHora();
    }
}
