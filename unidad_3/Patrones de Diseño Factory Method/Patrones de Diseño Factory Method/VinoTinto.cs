﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Patrones_de_Diseño_Factory_Method
{
    class VinoTinto : BebidaEmbriagante
    {
        public override int CuentoMeEmbriagaPorHora()
        {
            return 20;
        }
    }
}
